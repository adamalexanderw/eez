<?php
	$pageTitle = "Cleaner Radius";
	$page = "cleaner-radius";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-8 medium-12 column">
			<section>
				<h2 class="dark-grey">We need some more details before you can start cleaning</h2>
				<hr>

				<div class="row mt3">
					<div class="medium-12 column">
						<h3 class="primary">1. Tell us what areas you can clean</h3>
					</div>
				</div>

				<div class="row">
					<div class="medium-12 column">
						<h5>Enter a post code for where you will be based</h5>
						<input type="text" placeholder="Post Code" />
					</div>
				</div>

				<div class="row mt3">
					<div class="large-8 medium-10 column">
						<h5>Toggle Radius</h5>
						<div class="row">
							<div class="medium-10 small-9  column">
								<div class="slider" data-slider data-initial-start="10" data-step="5" data-end="50" data-position-value-function="log" data-non-linear-base="5">
									<span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="radius-slider"></span>
								</div>
							</div>
							<div class="medium-2 small-3 column">
								<input type="number" id="radius-slider">
							</div>
						</div>
					</div>
				</div>

				<a href="<?php print HTTP; ?>/cleaner-pricing" class="continue btn-primary mt4">Continue</a>
			</section>
		</div>

		<aside>
			<div class="responsive-embed">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d75670.72375005244!2d-1.8577401034621632!3d53.65214080836941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487962132bcdb7bb%3A0x653c3a498c896a17!2sHuddersfield!5e0!3m2!1sen!2suk!4v1549989510941" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</aside>
	</div>
</form>

<?php include('../includes/footer.php'); ?>