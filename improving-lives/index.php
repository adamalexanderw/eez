<?php
	$pageTitle = "Improving Lives";
	$page = "improving-lives";
	include('../includes/header.php');
?>

<div class="hero" style="background-image: url('<?php print HTTP; ?>dist/assets/img/improving-lives.jpg');">
	<div class="caption">
		<div class="row">
			<div class="large-8 medium-10 medium-centered column">
				<h1 class="fade-in"><?= $pageTitle; ?></h1>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2>1 in 10 people worldwide don’t have access to clean water.</h2>
			<p>At Eez, we want to stop that. We’re on a mission to help those in need. Water is something that we should all have as a everyday essential, wherever and whoever we are.</p>

			<p>With every clean, we’ll donate 10% of our profits to charity water, to help them on their mission to elimate water shortage in developing countries.</p>
		</div>
	</div>
</section>

<section class="grey slant-top">
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2>Clean water changes everything</h2>
		</div>
	</div>
	<div class="row small-up-1 medium-up-2 large-up-4 mt4 text-center">
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/clean_water.svg" width="250" alt="Sign Up">
			<h4>Health</h4>
			<p>Diseases from dirty water kill more people every year than all forms of violence, including war.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/time.svg" width="250" alt="Saving Time">
			<h4>Time</h4>
			<p>In Africa alone, women spend 40 billion hours a year walking for water.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/education.svg" width="250" alt="Education">
			<h4>Education</h4>
			<p>Clean water helps keep kids in school, especially girls. Less time collecting water means more time in class.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/women_empowerment.svg" width="250" alt="Woment Empowerment">
			<h4>Empowerment</h4>
			<p>Women are responsible for 72% of the water collected in Sub-Saharan Africa.</p>
		</div>
	</div>
</section>

<div class="image-block slant-bottom" style="background-image: url('<?php print HTTP; ?>dist/assets/img/drinking.jpg');"></div>

<section>
	<div class="row">
		<div class="medium-6 column">
			<h2>It’s not just cleaning windows</h2>
			<p>By signing up to clyr, not only will we clean your windows, but with every order, we’ll donate 10% of our profits to charities tackling water shortage.</p>
			<p>As an organisation, we want to make a difference.</p>
			<p><a href="#" class="btn-primary mt3">Sign Up Now</a></p>
		</div>
		<div class="medium-4 medium-offset-1 column">
			<img src="<?php print HTTP; ?>/dist/assets/img/clean.svg">
		</div>
	</div>
</section>

<?php include('../includes/cta.php'); ?>

<?php include('../includes/footer.php'); ?>