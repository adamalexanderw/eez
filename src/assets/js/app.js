import $ from 'jquery';
import whatInput from 'what-input';
import Swiper from 'swiper';

window.$ = $;

//import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';
import './lib/custom';

$(document).ready(function(){
	$(document).foundation();

	$('#offCanvas').on('opened.zf.offcanvas', function(){
		$('.toggle').toggleClass("open");
	});

	$('#offCanvas').on('closed.zf.offcanvas', function(){
		$('.toggle').toggleClass("open");
	});
});
