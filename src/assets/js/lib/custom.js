import $ from 'jquery';
import Swiper from 'swiper';

function swiper() {
	var featuresSwiper = new Swiper ('.features-swiper', {
	    loop: true,
	    centeredSlides: true,
	    speed: 600,
	    navigation: {
			nextEl: '.next',
			prevEl: '.prev',
		},
  	});
}

function smooth_scroll() {
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
		    $('html, body').animate({
		      scrollTop: target.offset().top
		    }, 1000);
		    return false;
		  }
		}
	});
}

function show_details() {
	$('#details').on('on.zf.toggler', function(){
		$('#toggler').html('<i class="fas fa-fw fa-plus-circle"></i> Show Details');
	});

	$('#details').on('off.zf.toggler', function(){
		$('#toggler').html('<i class="fas fa-fw fa-minus-circle"></i> Hide Details');
	});
}

function animate() {
	$('.fade-in').each(function(){
		 var divPos = $(this).offset().top,
             topOfWindow = $(window).scrollTop();

		 if( divPos < topOfWindow+600 ){
			$(this).addClass('active');
		 }
	});
}

$(window).scroll(function(){
	animate();
});

$(document).ready(function(){
	swiper();
	show_details();
	smooth_scroll();
	animate();
});
