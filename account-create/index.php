<?php
	$pageTitle = "Create Account";
	$page = "create-account";
	include('../includes/header.php');
?>


<section>
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2 class="fade-in">Lets create your account.<br>It only takes 2 minutes.</h2>
		</div>
	</div>
	<div class="row mt3">
		<div class="large-5 medium-7 medium-centered column">
			<form class="create-account">
				<h5>Create a username</h5>
				<input class="input-group-field" placeholder="Username" type="text">
				<h5>Create a password</h5>
				<input class="input-group-field" placeholder="Password" type="password">
				<input class="input-group-field" placeholder="Confirm Password" type="password">

				<input type="submit" class="btn-secondary block mt2" value="Log In">
			</form>
		</div>
		<div class="row column text-center mt3">
			<a href="<?php print HTTP; ?>/cleaner-radius" class="text-center">Next Stage &rarr;</a>
		</div>
	</div>
</section>

<?php include('../includes/footer.php'); ?>