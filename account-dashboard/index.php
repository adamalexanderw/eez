<?php
	$pageTitle = "Your Account";
	$page = "account-dashboard";
	include('../includes/header.php');
?>

<section>
	<div class="row">
		<div class="large-12 column">
			<h2 class="fade-in">Your Account</h2>
		</div>
	</div>
	<hr>
	<div class="row mt4">
		<div class="large-4 medium-5 column">
			<div class="card">
				<div class="content">
					<h4 class="mb3">Hey Oli,</h4>
					<p><span class="primary">Your next clean is</span>
					<br>28th March</p>
					<a href="#" class="btn-secondary">Logout</a>
				</div>
			</div>
		</div>
		<div class="large-8 medium-7 column">
			<div class="row">
				<div class="large-12 column">
					<div class="account-box">
						<a href="#" class="edit">Change / Stop Cleaner</a>
						<h4>Your Cleaner</h4>

						<div class="row mt3">
							<div class="large-2 column">
								<img src="<?php print HTTP; ?>dist/assets/img/placeholder.png" class="show-for-large" alt="Eez Placeholder">
							</div>
							<div class="large-10 column">
								<h4 class="cleaner">Last of the summer shine</h4>
								<ul class="menu">
									<li>Clean price: £10</li>
									<li>Write a review</li>
									<li>Send message</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Your email</h4>
						<p>oli@oli-smith.com</p>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Your password</h4>
						<p>•••••••••••</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Change frequency</h4>
						<p>Every Month</p>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Your address</h4>
						<p>2-2A Brook’s Yard<br>
						Huddersfield<br>
						West Yorkshire<br>
						HD11RL</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Billing Info</h4>
						<p>View</p>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Cancel / Pause</h4>
						<p>Cancel your subscription?</p>
					</div>
				</div>
			</div>

			<div class="recommend">
				<div class="row">
					<div class="large-7 medium-10 column">
						<h3>Recommend a friend</h3>
						<p>Know someone on your street who could benefit from the Eez service? We’ll give you two free cleans if  you recommend us to a friend.</p>
						<form class="unique-code">
							<div class="input-group">
								<input class="input-group-field" placeholder="Send a unique code"  type="text">
								<div class="input-group-button">
									<button class="btn-primary" type="button">
										<i class="far fa-paper-plane"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('../includes/footer.php'); ?>