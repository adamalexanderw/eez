<?php
	$pageTitle = "Cleaner Schedule";
	$page = "cleaner-schedule";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-8 medium-12 column">
			<section>
				<h2>Cleaning Schedule</h2>
				<hr>

				<div class="row mt3">
					<div class="medium-10 column">
						<h4>Jobs that need completing this week</h4>
						<p>To create a round, select the jobs you want to complete</p>
					</div>
				</div>

				<div class="row">
					<div class="medium-10 column">
						<table class="rounds mt2">
							<tr>
								<td>Job 1</td>
								<td colspan="2">Address 1</td>
								<td>£12</td>
								<td>
									<span class="checkbox-wrap"><label>
										<input type="checkbox" value="1">
										<span class="fake-tick"></span>
									</label></span>
								</td>
							</tr>
							<tr>
								<td>Job 1</td>
								<td colspan="2">Address 1</td>
								<td>£12</td>
								<td>
									<span class="checkbox-wrap"><label>
										<input type="checkbox" value="1">
										<span class="fake-tick"></span>
									</label></span>
								</td>
							</tr>
							<tr>
								<td>Job 1</td>
								<td colspan="2">Address 1</td>
								<td>£12</td>
								<td>
									<span class="checkbox-wrap"><label>
										<input type="checkbox" value="1">
										<span class="fake-tick"></span>
									</label></span>
								</td>
							</tr>
							<tr>
								<td>Job 1</td>
								<td colspan="2">Address 1</td>
								<td>£12</td>
								<td>
									<span class="checkbox-wrap"><label>
										<input type="checkbox" value="1">
										<span class="fake-tick"></span>
									</label></span>
								</td>
							</tr>
						</table>

						<a href="#" class="btn-primary mt3">Create Round +</a>
					</div>
				</div>
				<hr>

				<div class="row mt2">
					<div class="medium-10 column">
						<h4>Your Rounds</h4>
					</div>
				</div>

				<div class="row">
					<div class="medium-10 column">
						<table class="rounds mt3">
							<tr>
								<td colspan="2">Round 1 (Monday)</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Print</a></td>
							</tr>
							<tr>
								<td colspan="2">Round 2 (Tuesday)</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Print</a></td>
							</tr>
							<tr>
								<td colspan="2">Round 3 (Wednesday)</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Print</a></td>
							</tr>
						</table>
					</div>
				</div>
			</section>
		</div>

		<aside>
			<div class="responsive-embed">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d75670.72375005244!2d-1.8577401034621632!3d53.65214080836941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487962132bcdb7bb%3A0x653c3a498c896a17!2sHuddersfield!5e0!3m2!1sen!2suk!4v1549989510941" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</aside>
	</div>
</form>

<?php include('../includes/footer.php'); ?>