<?php define("HTTP", ($_SERVER["SERVER_NAME"] == "localhost")
   ? "http://localhost:8080/aye_creative/eez/"
   : "http://eez.aye.agency/"
); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Eez. The new way to get your windows cleaned." />
    <meta name="keywords" content="" />
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico?v1">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <title>Eez | The new way to get your windows cleaned | <?= $pageTitle ?></title>


    <link href="<?php print HTTP; ?>dist/assets/css/app.css?v1" rel="stylesheet">
    <script src="<?php print HTTP; ?>dist/assets/js/app.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

<body class="<?= $page; ?>" name="top">
    <div class="off-canvas position-right" id="offCanvas" data-off-canvas>
        <ul class="vertical menu mobile-menu" data-responsive-menu="accordion">
            <li><a href="<?php print HTTP; ?>">Home</a></li>
            <li><a href="<?php print HTTP; ?>/how-it-works">How It Works</a></li>
            <li><a href="<?php print HTTP; ?>/improving-lives">Improving Lives</a></li>
            <li>
                <a href="<?php print HTTP; ?>/checkout-order" class="mt3"><i class="fas fa-fw fa-edit"></i> Sign Up</a>
                <ul>
                    <li><a href="<?php print HTTP; ?>/checkout-order">Get your windows cleaned</a></li>
                    <li><a href="<?php print HTTP; ?>/cleaner-account">Become a window cleaner</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php print HTTP; ?>/account-login"><i class="fas fa-fw fa-user"></i> Log In</a>
                <ul>
                    <li><a href="<?php print HTTP; ?>/account-dashboard">I'm a customer of eez</a></li>
                    <li><a href="<?php print HTTP; ?>/cleaner-dashboard">I'm a cleaner</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="off-canvas-content" data-off-canvas-content>
        <div class="page-wrap" id="body-top">
            <header class="banner">
                <nav id="site-navigation" class="main-navigation top-bar" role="navigation">
                    <div class="row">
                        <div class="large-3 medium-5 small-8 column">
                            <div class="top-bar-left">
                                <ul class="menu">
                                    <li class="logo">
                                        <a href="<?php print HTTP; ?>" rel="home">
                                            <img src="<?php print HTTP; ?>dist/assets/img/eez-white.svg" class="show-for-large" width="100" alt="Eez Window Cleaning">
                                            <img src="<?php print HTTP; ?>dist/assets/img/eez.svg" class="hide-for-large" width="100" alt="Eez Window Cleaning">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="large-9 medium-0 small-4 column">
                            <div class="hide-for-large">
                                <a href="#" class="toggle" data-toggle="offCanvas">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </a>
                            </div>

                            <ul class="vertical medium-horizontal menu show-for-large desktop-menu" data-responsive-menu="accordion large-dropdown" data-disable-hover="true" data-click-open="true">
                                <li><a href="<?php print HTTP; ?>/how-it-works">How It Works</a></li>
                                <li><a href="<?php print HTTP; ?>/improving-lives">Improving Lives</a></li>
                                <li>
                                    <a href="#" class="sign-up">Sign Up</a>
                                    <ul>
                                        <li><a href="<?php print HTTP; ?>/checkout-order">Get your windows cleaned</a></li>
                                        <li><a href="<?php print HTTP; ?>/cleaner-account">Become a window cleaner</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php print HTTP; ?>/account-login" class="log-in ml1"><i class="fas fa-fw fa-user"></i> Log In</a>
                                    <ul>
                                        <li><a href="<?php print HTTP; ?>/account-dashboard">I'm a customer of eez</a></li>
                                        <li><a href="<?php print HTTP; ?>/cleaner-dashboard">I'm a cleaner</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>

