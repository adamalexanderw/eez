<section class="mission slant-top">
	<div class="row">
		<div class="medium-6 medium-offset-6 column">
			<h2>It’s not just cleaning windows</h2>
			<p>At Eez, we aren’t just about cleaning windows. We believe in making a difference to the wider world. With every house we clean, we’ll provide fresh water for those who need it.</p>
			<a href="#" class="btn-primary mt3">Find out more</a>
		</div>
	</div>
</section>
