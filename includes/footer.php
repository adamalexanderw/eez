<footer>
	<div class="row">
		<div class="medium-3 small-6 column">
			<ul class="vertical menu">
                <li><a href="<?php print HTTP; ?>">Home</a></li>
                <li><a href="<?php print HTTP; ?>/how-it-works">How It Works</a></li>
                <li><a href="<?php print HTTP; ?>/improving-lives">Improving Lives</a></li>
            </ul>
		</div>
		<div class="medium-3 small-6 column">
			<ul class="vertical menu">
                <li><a href="<?php print HTTP; ?>/login">Login</a></li>
                <li><a href="<?php print HTTP; ?>/sign-up">Sign Up</a></li>
            </ul>
		</div>
		<div class="medium-3 column">
			<p class="mb0">Follow Us</p>
            <ul class="horizontal menu mt1">
                <li><a href="#"><i class="fab fa-fw fa-lg fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-fw fa-lg fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-fw fa-lg fa-facebook-f"></i></a></li>
            </ul>
		</div>
		<div class="medium-3 column">
			<ul class="vertical menu">
                <li><a href="#top">Back To Top</a></li>
            </ul>
		</div>
	</div>

	<div class="lower-footer">
		<div class="row">
			<div class="small-6 column">
				<p>Company info. Registered company etc</p>
			</div>
			<div class="small-6 text-right column">
				<p>Eez Window Cleaning</p>
			</div>
		</div>
	</div>
</footer>

</body>
</html>