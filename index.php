<?php
	$pageTitle = "Home";
	$page = "home";
	include('includes/header.php');
?>

<div class="home hero">
	<div class="caption">
		<div class="row">
			<div class="large-8 medium-10 medium-centered column">
				<h1 class="fade-in x1">The new way to get<br class="hide-for-small-only"> your windows cleaned</h1>
				<p class="fade-in x2">Enter your postcode to see if we cover your area</p>

				<form class="postcode-search fade-in x3">
					<div class="input-group">
						<span class="input-group-label"><i class="fas fa-search fa-lg"></i></span>
						<input class="input-group-field" placeholder="Enter your postcode"  type="text">
						<div class="input-group-button">
							<a href="<?php print HTTP; ?>checkout-order" class="btn-primary">Search</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<section >
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2>Signing up is easy</h2>
		</div>
	</div>
	<div class="row small-up-1 medium-up-2 large-up-3 mt4 text-center">
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/book_your_slot.svg" width="250" alt="Book your slot">
			<h4>Book your slot</h4>
			<p>Choose the amount of windows you have, and when you want us to come and clean.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/stay_in_touch.svg" width="250" alt="We’ll stay in touch">
			<h4>We’ll stay in touch</h4>
			<p>We’ll let you know when we’re coming, and when we’ve done. No unexpected visits!</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/cancel_anytime.svg" width="250" alt="Cancel anytime">
			<h4>Cancel anytime</h4>
			<p>We’ll keep on cleaning until you want us to stop! Unless you want to pause, or cancel.</p>
		</div>
	</div>
	<div class="row mt4">
		<div class="large-8 medium-10 medium-centered text-center column">
			<a href="#" class="btn-primary">Sign Up Today</a>
		</div>
	</div>
</section>

<?php include('includes/mission.php'); ?>

<?php include('includes/footer.php'); ?>