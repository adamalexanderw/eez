<?php
	$pageTitle = "Cleaner Profile";
	$page = "cleaner-profile";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-9 medium-12 column">
			<section>
				<h2 class="dark-grey">Tell us a little about yourself<br>for your eez profile</h2>
				<hr>
				<div class="row">
					<div class="medium-8 column">
						<h3 class="primary">Your Details</h3>
					</div>
				</div>

				<div class="row mt3">
					<div class="large-10 column">
						<h5>Enter your display name</h5>
						<input type="email" placeholder="Email">
					</div>
				</div>


				<div class="row mt3">
					<div class="large-10 column">
						<h5>Write a bio, be as personal as you want to be</h5>
						<textarea rows="6" placeholder="Tell us about yourself"></textarea>
					</div>
				</div>

				<div class="row mt3">
					<div class="large-10 column">
						<h5>Upload a photo for your profile</h5>
						<p>Max size 500px x 500px</p>
						<input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
					</div>
				</div>

				<a href="<?php print HTTP; ?>/cleaner-dashboard" class="continue btn-primary mt4">Continue</a>
			</section>
		</div>
	</div>
</form>

<?php include('../includes/footer.php'); ?>