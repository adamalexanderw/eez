<?php
	$pageTitle = "Cleaner Radius";
	$page = "cleaner-radius";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-8 medium-12 column">
			<section>
				<h2 class="dark-grey">Enter some estimates for cleaning the following houses. You can change these later.</h2>
				<hr>

				<div class="row mt3">
					<div class="medium-12 column">
						<h3 class="primary">2. Tell us how much you would charge</h3>
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-3 column">
						<a class="btn-hollow">
							<img src="<?php print HTTP; ?>dist/assets/img/icons/house_1.svg" alt="2-12 Windows" width="250">
						</a>
					</div>
					<div class="medium-9 column">
						<h4>How much would you charge for a house with 2-12 windows?</h4>
						<div class="row mt3">
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Min Price" />
								</div>
							</div>
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Max Price" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>

				<div class="row">
					<div class="medium-3 column">
						<a class="btn-hollow">
							<img src="<?php print HTTP; ?>dist/assets/img/icons/house_2.svg" alt="2-12 Windows" width="250">
						</a>
					</div>
					<div class="medium-9 column">
						<h4>How much would you charge for a house with 13-26 windows?</h4>
						<div class="row mt3">
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Min Price" />
								</div>
							</div>
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Max Price" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>

				<div class="row">
					<div class="medium-3 column">
						<a class="btn-hollow">
							<img src="<?php print HTTP; ?>dist/assets/img/icons/house_3.svg" alt="2-12 Windows" width="250">
						</a>
					</div>
					<div class="medium-9 column">
						<h4>How much would you charge for a house with 27-40 windows?</h4>
						<div class="row mt3">
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Min Price" />
								</div>
							</div>
							<div class="medium-6 column">
								<div class="input-group">
									<span class="input-group-label">£</span>
									<input type="text" placeholder="Max Price" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<a href="<?php print HTTP; ?>/cleaner-profile" class="btn-primary continue mt4">Continue</a>
			</section>
		</div>
	</div>
</form>

<?php include('../includes/footer.php'); ?>