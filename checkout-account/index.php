<?php
	$pageTitle = "Account";
	$page = "checkout-account";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-8 medium-12 column">
			<section>
				<h2><span class="dark-grey">Confirm your personal details</span></h2>
				<hr>

				<div class="row mt3">
					<div class="medium-6 column">
						<h3 class="primary">Your Details</h3>
					</div>
				</div>

				<div class="row">
					<div class="small-6 column">
						<input type="text" placeholder="First name" />
					</div>
					<div class="small-6 column">
						<input type="text" placeholder="Last name" />
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-12 column">
						<h5>To log in and manage your account, we need your email</h5>
						<input type="email" placeholder="Email Address" />
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-12 column">
						<h5>Free notifications to let you know when we’re coming to clean</h5>
						<input type="tel" placeholder="Mobile Number" />
					</div>
				</div>

				<a href="<?php print HTTP; ?>/checkout-success" class="continue btn-primary mt4">Continue</a>
			</section>
		</div>

		<aside>
			<div class="responsive-embed">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d75670.72375005244!2d-1.8577401034621632!3d53.65214080836941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487962132bcdb7bb%3A0x653c3a498c896a17!2sHuddersfield!5e0!3m2!1sen!2suk!4v1549989510941" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</aside>
	</div>
</form>

<?php include('../includes/footer.php'); ?>