<?php
	$pageTitle = "Success";
	$page = "checkout-success";
	include('../includes/header.php');
?>

<form class="checkout">
	<section>
		<div class="row">
			<div class="large-7 medium-9 column">
				<h2><span class="dark-grey">Success!</span><br>Your windows will be clean soon!</h2>
				<hr>

				<div class="row">
					<div class="medium-12 column">
						<h5>Is there anything we need to know?</h5>
						<textarea rows="8">Enter Additional Notes</textarea>
						<a href="#" class="btn-secondary mt3 mb3">Save</a>
					</div>
				</div>

			</div>

			<div class="large-4 medium-3 column">
				<div class="card">
					<img src="<?php print HTTP; ?>dist/assets/img/water.jpg" class="hide-for-small-only">
					<div class="content">
						<h4>Thanks!</h4>
						<p>Your subscription will provide clean water for 700 people every month. Find out more about how we’re trying to make a difference.</p>
						<a href="<?php print HTTP; ?>/improving-lives/" class="btn-secondary">Find out more</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</form>

<?php include('../includes/footer.php'); ?>