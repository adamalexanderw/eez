<?php
	$pageTitle = "Choose Cleaners";
	$page = "checkout-cleaners";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-9 medium-12 column">
			<section>
				<h2>Yay<br><span class="dark-grey">There's cleaners available in your area.</span></h2>
				<hr>
				<div class="row">
					<div class="medium-8 column">
						<h3 class="primary">Choose a cleaner</h3>
					</div>
				</div>

				<div class="row">
					<div class="large-10 column">
						<div class="cleaner mt3">
							<div class="row">
								<div class="medium-2 column hide-for-small-only">
									<img src="<?php print HTTP; ?>dist/assets/img/placeholder.png" alt="Eez Placeholder">
								</div>
								<div class="medium-10 column">
									<div class="row">
										<div class="medium-8 column">
											<h4 class="cleaner">Last of the summer shine</h4>
										</div>
										<div class="medium-4 column">
											<h5 class="review">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star-half"></i>
											</h5>
										</div>
									</div>
									<div class="row">
										<div class="medium-6 column">
											<p><strong>Clean price:</strong><br>
											Between £10-£25</p>
										</div>
										<div class="medium-6 column">
											<a href="#" class="btn-primary mt2">Choose Cleaner</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr>

						<div class="cleaner mt3">
							<div class="row">
								<div class="medium-2 column hide-for-small-only">
									<img src="<?php print HTTP; ?>dist/assets/img/placeholder.png" alt="Eez Placeholder">
								</div>
								<div class="medium-10 column">
									<div class="row">
										<div class="medium-8 column">
											<h4 class="cleaner">Alex's Wash &amp; Dry</h4>
										</div>
										<div class="medium-4 column">
											<h5 class="review">
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
											</h5>
										</div>
									</div>
									<div class="row">
										<div class="medium-6 column">
											<p><strong>Clean price:</strong><br>
											Between £10-£25</p>
										</div>
										<div class="medium-6 column">
											<a href="#" class="btn-primary mt2">Choose Cleaner</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr>

					</div>
				</div>

				<a href="<?php print HTTP; ?>/checkout-address" class="continue btn-primary mt4">Continue</a>
			</section>
		</div>
	</div>
</form>

<?php include('../includes/footer.php'); ?>