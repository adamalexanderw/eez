<?php
	$pageTitle = "Login";
	$page = "login";
	include('../includes/header.php');
?>


<section>
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2 class="fade-in">Welcome Back</h2>
		</div>
	</div>
	<div class="row mt4">
		<div class="large-5 medium-7 medium-centered column">
			<form class="login">
				<input class="input-group-field" placeholder="Email"  type="text">
				<input class="input-group-field" placeholder="Password"  type="text">
				<p><a href="#">Forgotten Password?</a></p>

				<input type="submit" class="btn-secondary block" value="Log In">

				<p class="mt3">Don’t have an account yet?<br>You’ll need to <a href="#">sign up.</a></p>
			</form>
		</div>
	</div>
</section>

<?php include('../includes/footer.php'); ?>