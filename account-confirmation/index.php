<?php
	$pageTitle = "Account Confirmation";
	$page = "account-confirmation";
	include('../includes/header.php');
?>

<section>
	<div class="row">
		<div class="large-8 medium-10 column">
			<h2 class="dark-grey">Thanks for signing up. We've just sent you an email. Confirm your account to finish the sign up process.</h2>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="large-8 medium-7 column">
			<div class="recommend">
				<div class="row">
					<div class="large-7 medium-10 column">
						<h3>Recommend a friend</h3>
						<p>Know someone on your street who could benefit from the Eez service? We’ll give you two free cleans if  you recommend us to a friend.</p>
						<form class="unique-code">
							<div class="input-group">
								<input class="input-group-field" placeholder="Send a unique code"  type="text">
								<div class="input-group-button">
									<button class="btn-primary" type="button">
										<i class="far fa-paper-plane"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('../includes/footer.php'); ?>