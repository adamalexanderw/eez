<?php
	$pageTitle = "Cleaner Dashboard ";
	$page = "cleaner-dashboard";
	include('../includes/header.php');
?>

<section>
	<div class="row">
		<div class="large-12 column fade-in">
			<h2>Your Account</h2>
			<p>Last of the summer shine</p>
		</div>
	</div>
	<hr>
	<div class="row mt4">
		<div class="large-4 medium-5 column">
			<div class="card">
				<div class="content">
					<h4 class="mb4">Hey Oli,</h4>
					<a href="#" class="btn-secondary mt4">Logout</a>
				</div>
			</div>
		</div>
		<div class="large-8 medium-7 column">
			<div class="row">
				<div class="large-12 column">
					<div class="account-box">
						<h4>At a glance</h4>

						<div class="row mt4 mb4">
							<div class="large-3 column">
								<h5>113</h5>
								<p>Customers</p>
							</div>
							<div class="large-4 column">
								<h5>£200,103</h5>
								<p>Revenue made since using eez</p>
							</div>
							<div class="large-4 end column">
								<h5 class="review">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star-half"></i>
								</h5>
								<p>Your Average Rating</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Your details</h4>
						<p>oli@oli-smith.com</p>

						<a href="#" class="btn-primary mt3">View</a>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Your password</h4>
						<p>•••••••••••</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<h4>Your Jobs</h4>
						<p>See all your live jobs</p>

						<a href="#" class="btn-primary mt3">View</a>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<h4>Cleaning Schedule</h4>
						<p>Create a schedule for your work</p>

						<a href="<?php print HTTP; ?>/cleaner-schedule" class="btn-primary mt3">View</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Billing Info</h4>
						<p>View</p>
					</div>
				</div>
				<div class="large-6 column">
					<div class="account-box">
						<a href="#" class="edit">Edit</a>

						<h4>Cancel / Pause</h4>
						<p>Cancel your subscription?</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('../includes/footer.php'); ?>