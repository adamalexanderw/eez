<?php
	$pageTitle = "How it works";
	$page = "how-it-works";
	include('../includes/header.php');
?>

<div class="hero">
	<div class="caption">
		<div class="row">
			<div class="large-8 medium-10 medium-centered column">
				<h1 class="fade-in"><?= $pageTitle; ?></h1>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2>At Eez, we think there’s a better way to get your windows cleaned.</h2>
			<p>We connect customers, with window cleaners. Simply book your slot, choose how often you want us to come, and we’ll keep coming until you want us to stop.</p>
		</div>
	</div>
</section>

<section class="grey slant-top">
	<div class="row">
		<div class="large-8 medium-10 medium-centered text-center column">
			<h2 class="mb0">How Eez Works</h2>
		</div>
	</div>
	<div class="row small-up-1 medium-up-2 large-up-4 mt4 text-center">
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/book_your_slot.svg" width="250" alt="Sign Up">
			<h4>Sign up</h4>
			<p>Creating an account takes minutes. We need to know how many windows we’re cleaning and when you want us to come.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/stay_in_touch.svg" width="250" alt="We’ll stay in touch">
			<h4>We’ll stay in touch</h4>
			<p>We’ll tell you when we are coming, and when the jobs been done, unlike your old window cleaner…</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/cancel_anytime.svg" width="250" alt="Cancel anytime">
			<h4>Cancel anytime</h4>
			<p>We’ll keep on cleaning until you want us to stop! Unless you want to pause, or cancel.</p>
		</div>
  		<div class="column column-block">
  			<img src="<?php print HTTP; ?>dist/assets/img/icons/improving_lives.svg" width="250" alt="Improving Lives">
			<h4>Improving lives</h4>
			<p>With every window clean we provide fresh water to those in need. It’s more than just cleaning windows.</p>
		</div>
	</div>
</section>

<div class="image-block slant-bottom" style="background-image: url('<?php print HTTP; ?>dist/assets/img/plant.jpg');"></div>

<section>
	<div class="row">
		<div class="medium-12 column">
			<div class="features-swiper swiper-container">
		   		<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="row">
							<div class="medium-6 column">
								<h3 class="mb4">Key Features</h3>
								<h2>No contract, you can cancel anytime</h2>
								<p>No contract. No pressure. If you want to stop the service, simply log in to cancel your order.</p>
							</div>
							<div class="medium-5 medium-offset-1 column">
								<img src="<?php print HTTP; ?>/dist/assets/img/contract.svg">
							</div>
						</div>
					</div>
			    </div>

			    <div class="nav">
			        <div class="prev"><img src="<?php print HTTP; ?>/dist/assets/img/arrow.svg" width="50"></div>
					<div class="next"><img src="<?php print HTTP; ?>/dist/assets/img/arrow.svg" width="50"></div>
			    </div>
			</div>
		</div>
	</div>
</section>

<?php include('../includes/cta.php'); ?>

<?php include('../includes/footer.php'); ?>