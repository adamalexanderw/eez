<?php
	$pageTitle = "Order";
	$page = "checkout-order";
	include('../includes/header.php');
?>

<form class="checkout">
	<div class="row">
		<div class="large-9 medium-12 column">
			<section>
				<h2><span class="dark-grey">Great news! We cover your area.</span><br>Let’s get started.</h2>
				<hr>
				<div class="row">
					<div class="medium-8 column">
						<h3 class="primary">Tell us how many windows we’re coming to clean</h3>
					</div>
					<div class="medium-3 medium-offset-1 column">
						<h5><i class="fas fa-xs fa-question-circle"></i> How do i count my windows</h5>
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-12 column">
						<h5>Do you have a conservatory?</h5>
						<a class="btn-hollow mr1">Yes</a>
						<a class="btn-hollow">No</a>
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-12 column">
						<h5>Choose the amount of windows in your house</h5>

						<div class="windows row small-up-1 medium-up-2 large-up-4">
  							<div class="column column-block">
								<a class="btn-hollow">
									<img src="<?php print HTTP; ?>dist/assets/img/icons/house_1.svg" alt="2-12 Windows"><br>
									2-12 Windows
								</a>
							</div>
  							<div class="column column-block">
								<a class="btn-hollow">
									<img src="<?php print HTTP; ?>dist/assets/img/icons/house_2.svg" alt="2-12 Windows"><br>
									13-26 Windows
								</a>
							</div>
  							<div class="column column-block">
								<a class="btn-hollow">
									<img src="<?php print HTTP; ?>dist/assets/img/icons/house_3.svg" alt="2-12 Windows"><br>
									19-27 Windows
								</a>
							</div>
  							<div class="column column-block">
								<a class="btn-hollow">
									<img src="<?php print HTTP; ?>dist/assets/img/icons/house_4.svg" alt="2-12 Windows"><br>
									Large/Commercial
								</a>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="row mt3">
					<div class="medium-12 column">
						<h3 class="primary">How many floors do you have?</h3>
						<a class="btn-hollow mr1">1</a>
						<a class="btn-hollow mr1">2</a>
						<a class="btn-hollow">3</a>
						<a class="btn-hollow">4 or more</a>
					</div>
				</div>

				<hr>

				<div class="row mt3">
					<div class="medium-6 column">
						<h3 class="primary">Choose your first clean</h3>
						<input type="date" id="start" name="trip" value="2018-07-22" min="2018-01-01" max="2020-12-31" />
					</div>
				</div>

				<div class="row mt3">
					<div class="medium-12 column">
						<h5>How often do you want them cleaned?</h5>
						<a class="btn-hollow mr1">Every Week</a>
						<a class="btn-hollow mr1">Every 2 Weeks</a>
						<a class="btn-hollow">Every Month</a>
					</div>
				</div>

				<a href="<?php print HTTP; ?>/checkout-cleaners" class="continue btn-primary mt4">Continue</a>
			</section>
		</div>
	</div>
</form>

<?php include('../includes/footer.php'); ?>